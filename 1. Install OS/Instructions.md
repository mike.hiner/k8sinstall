# Install OS

####Synopsis
Image the sdcard with Ubuntu 18.04.  This is specific to the XU4 but the equivalent process would need to occur for other platforms.

####Result
A Linux operating system with a unique hostname and SSH server installed

####Steps
**Perform all steps on every machine that will be part of cluster**
1. Download the ubuntu minimal image (currently ubuntu-18.04-4.14-minimal-odroid-xu4-20180531.img) https://wiki.odroid.com/odroid-xu4/os_images/linux/ubuntu_4.14/ubuntu_4.14
2. Copy image onto sdcard, sorry not sure how to do it on anything but linux ;)
    `sudo dd bs=1M if=ubuntu-18.04-4.14-minimal-odroid-xu4-20180531.img of=/dev/mmcblk0`
1. insert sdcard into xu4 (make sure the switch on the side is set to sdcard, by default it is set to eMMC)
1. Power on and let it run.  in around a minute it will shutdown automatically.  
1. Restart the XU4
1. Login iwth username root and password odroid
1. Find the ip address of the box 'ip a'

# Install Dependencies

####Synopsis
In this step we install  the various dependencies needed as well as some useful tools. Except where specifically noted these instructions should work on any recent (17.04 +) Ubuntu install.

####Result
A Linux operating system with the kubernetes applications installed. 

####Steps
**Perform all steps on every machine that will be part of cluster**
1. Update the OS
    ```
    apt-get update
    
    apt-get dist-upgrade
    ```
1. Reboot the server
    ```
    shutdown -r now
    ```
1. ssh or log back in to server
1. Remove outdated packages 
    ```
    apt autoremove
    ```
1. Install base packages
    1. `apt-transport-https ca-certificates curl software-properties-common` are needed to install other repositories
    1. `nfs-common` is needed so that we can use an NFS mount as a persistent volume
    1. `uuid-runtime` is needed to create a new machine-id later
    1. `tmux` is not needed but I prefer to install it to make multiple terminal sessins easier 
    ```
    apt-get install -y apt-transport-https ca-certificates curl software-properties-common nfs-common uuid-runtime tmux 

    ```
1. Install docker-ce 
    1. **NOTE:** the second command specifies arm.  If running on an x86 CPU replace `armhf` with `amd64` 
    ```
    sudo apt-get remove -y docker docker-engine docker.io
    
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    sudo add-apt-repository    "deb [arch=armhf] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
    
    apt-get update 
    
    apt-get install docker-ce
    ```
1. Test the docker install
    ```docker run --rm hello-world```
    i. When run this command will download a few images and then output text that starts with:
    ```
    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    ```
1. Create a non-root user and give them rights to run duco and docker commands:
    ```
    adduser kube
    # answer questions and set password
    usermod -a -G sudo kube
    usermod -a -G docker kube
    
    ```
1. Install the kubernetes applications (kubelet, kubeadm, and kubectl)
    ```
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    
    cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb http://apt.kubernetes.io/ kubernetes-xenial main
    EOF
    
    apt-get update; apt-get install -y kubelet kubeadm kubectl

    ```
 1. Set bridge-nf-call-iptables to 1.  This is needed to the k8s network overlay (flannel) to work
    ```
    sudo sysctl net.bridge.bridge-nf-call-iptables=1
    ```
 
 1. Update hostname (replace "kube0" to a unique value for each install)
    ```
    sed -i 's/odroid/kube0/g' /etc/hostname
    sed -i 's/odroid/kube0/g' /etc/hosts 
    ```
    
 1. **XU4 Specific step** The XU4 image uses a static machine-id for all servers.  The kubernetes networking uses the machine-id to identify each node in the k8s network however so we need to set it to a unique value
    ```
    # as root use uuidgen to create a new id
    uuidgen | sed 's/-//g' > /etc/machine-id
    ```
 1. reboot `shutdown -r now`
 ####Next Steps
 Create a kubernetes cluster