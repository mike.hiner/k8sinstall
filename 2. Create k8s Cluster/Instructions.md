# Install Master Node
####Synopsis
Bootstrap the master node using kubeadm, copy the client config to a non-root user, and install a k8s network
####Result
A running master node ready to accept worker nodes.
####Steps
1. Bootstrap the master node.  **NOTE**: in this case we are presuming the use of flannel for the network. 
    1. If you wish to use a different network see "Install a pod network add-on" here: https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
    1. On ARM the only networks I have had success with are flannel and Weave Net.  I have had more consistent results with flannel, thus its use in this process.
    1. in k8s 1.12 (scheduled release is in Sept 2018) it might be worth considering Kube-router instead.  Kube-router is included in 1.11 but my impression is that it is new and not quite seamless for deployments yet.
    ```
    kubeadm init --pod-network-cidr=10.244.0.0/16
    ```
    I personally like to add a few domain names to the certs but this is not required.  I find it makes it easier to access remotely without triggering insecure warnings.
    ```
    kubeadm init --apiserver-cert-extra-sans nano,nano.waddayawant.net,nano.conductofcode.com --pod-network cidr=10.244.0.0/16
    ```
1. The output from the step above should have two parts that we need to execute. (all substeps in this step are taken directly from the output of the previous command.)
    1. Copy the command that will be used to bootstrap the worker nodes.  The command will not be exactly what is below but will be similar. **IMPORTANT** Copy this somewhere safe since this command will be repeated on each worker node.  (In a production environment this instruction could be regenrated but that is beyond the scope of this instruction set.)
    ```
    You can now join any number of machines by running the following on each node as root:

    kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>
    ```
    1. Copy the client config to a non root user. 
        1. logout as root
        1. login as the non-root user created earlier
        1. Copy the client config (taken directly from the output of the `kubeadm init` command) 
    ```
    To start using your cluster, you need to run (as a regular user):
    
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```
1. Copy the flannel configuration to the master node
    ```
    scp flannel.yml root@<ip address>:~
    ```
1. login to the master node and Install the flannel network 
    ```
    kubectl apply -f flannel.yml
    
    ```
    
    * **NOTE** This yml file was modified to pull the arm specific images.  If installing on AMD64 the following command should be used:
    ```
    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml
    ```
1. Confirm all needed pods are up
    ```
    kubectl get pods --all-namespaces
    ```
    the output should look something like
    ```
    kube-system   coredns-78fcdf6894-4blcw                1/1       Running            1          29d
    kube-system   coredns-78fcdf6894-4nmzs                1/1       Running            1          29d
    kube-system   etcd-kube0                              1/1       Running            1          29d
    kube-system   kube-apiserver-kube0                    1/1       Running            1          29d
    kube-system   kube-controller-manager-kube0           1/1       Running            0          14d
    kube-system   kube-flannel-ds-cd9ll                   1/1       Running            5          29d
    kube-system   kube-flannel-ds-hdp99                   1/1       Running            3          29d
    kube-system   kube-flannel-ds-sspnt                   1/1       Running            3          29d
    kube-system   kube-flannel-ds-xsgwt                   1/1       Running            2          29d
    kube-system   kube-proxy-jzcj2                        1/1       Running            1          29d
    kube-system   kube-proxy-mqd22                        1/1       Running            1          29d
    kube-system   kube-proxy-tms8b                        1/1       Running            1          29d
    kube-system   kube-proxy-xjrz6                        1/1       Running            1          29d
    kube-system   kube-scheduler-kube0                    1/1       Running            0          14d

    ```
# Install Worker Nodes
####Synopsis
Install each worker node (otherwise known as copy and paste)
####Result
A minimal k8s cluster with one or more attached worker nodes.
####Steps
**Execute all steps for each worker node**
1. log in to the worker node as root
2. execute the `kubeadm join` command saved ealer
3. login as the non-root user on the master node
4. confirm the worker successfully joined
    ```
    kubectl get nodes
    ```
    the output should show all worker nodes as ready.  If not, try the command again until it shows as ready.  If this takes more than 5 min (probably 1) then something likely went wrong.
# Next Step

# References
1. kubeadmin bootstrapping https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
