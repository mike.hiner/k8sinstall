# First Deployment 

####Synopsis
Install a nginx container and configure it using a configMap
####Result
A deployed nginx server with json based logging

####Steps
1. Deploy the nginx config
    `kubectl apply -f ./nginx/nginx-config.yml`
1. Update the Deploy the nginx container itself
    `kubectl apply -f ./nginx/nginx.yml`
1. Confirm the deployment by connecting locally
    1. find the pod
    ```
    kubectl get pods
    ```
    1. forward the pod to your localhost
    ```
    kubectl port-forward <pod name> 8888:80
    ```
    1. go to `http://localhost:8888` and confirm that you get the default nginx page

# Install Traefik

####Synopsis
Install Traefik as an ingress controller and set up an ingress for the nginx deployment.

####Result
The ability to hit the nginx deployment by URI and add new ingresses.

####Steps
1. Add the roles needed for Traefik
    ```
    kubectl apply -f ./traefik/roles.yml

    ```
1. Create the Traefik daemonset
    ```
    kubectl apply -f ./traefik/daemonset.yml
    ```
1. Create an ingress to the traefik admin UI (not something I would recommend for production)
    **NOTE:** update the hostname if desired
    ```
    kubectl apply -f ./traefik/ingress.yml
    ```
1. Create an entry on your /etc/hosts (or equivalent for OS X/Windows) to point the host in the ingress to one of your worker nodes.
1. in a browser go to the host you specified
1. Create an ingress for the nginx server **NOTE:** Update the host if desired
    ```
    kubectl apply -f ./nginx/ingress.yml
    ```
1. Create an entry on your /etc/hosts (or equivalent for OS X/Windows) to point the host in the ingress to one of your worker nodes.
1. in a browser go to the host you specified

####Next Steps
 Fluentbits and centralized logging