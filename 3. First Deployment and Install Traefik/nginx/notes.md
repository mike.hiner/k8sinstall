To create a configmap from the actual config files:

`k create configmap myweb-config --from-file=raw_config/ -o yaml --dry-run > nginx-config.yml`
