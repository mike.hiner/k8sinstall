# Setup Elasticsearch

####Synopsis
Set up an elasticsearch server for use with fluentbits
####Result
An elasticsearch server running and ready to accept logs
####Steps
1. On the host for elasticsearch run (note this uses 6.3.2 whereas the current is 6.4.0 but I have not tested 6.4.0 yet):
    ```
    docker volume create elastic-data

    docker network create services-net

    docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -v elastic-data:/usr/share/elasticsearch/data --network services-net --name elastic docker.elastic.co/elasticsearch/elasticsearch:6.3.2
    ```
1. **Optional** install kibana (I would install kibana on k8s normally but have not done so yet)
    ```
    docker run -e ELASTICSEARCH_URL="http://elastic:9200" -e XPACK_MONITORING_UI_CONTAINER_ELASTICSEARCH_ENABLED=false --network services-net -p 0.0.0.0:5601:5601 docker.elastic.co/kibana/kibana:6.3.2
    ```
1. Create the endpoint that fluentd will use to connect to the Elasticsearch server
    ```
     docker run -e ELASTICSEARCH_URL="http://elastic:9200" -e XPACK_MONITORING_UI_CONTAINER_ELASTICSEARCH_ENABLED=false --network services-net -p 0.0.0.0:5601:5601 docker.elastic.co/kibana/kibana:6.3.2 
    ```
1. Update fluentbits/elastic-service.yml to point to your elasticsearch host
1. Create a endpoint for kubernetes to connect to the elasticsearch instance
    ```
    kubectl apply -f ./fluentbits/elastic-service.yml
    ```    
# Install Fluentbits

####Synopsis
In this step we install fluentbits on the k8s cluster and configure it to use elasticsearch for storage.
####Result
Logging for the Kubernetes cluster going to a centralized external Elasticsearch server.

####Steps
1. create the logging namespace
    ```
    kubectl create namespace logging
    ```
1. Set up account and permissions for fluentbits
    ```
    kubectl apply -f ./fluentbits/fluent-bit-service-account.yaml
    
    kubectl apply -f ./fluentbits/fluent-bit-role.yaml
    
    kubectl apply -f ./fluentbits/fluent-bit-role-binding.yaml
    ```
1. Set up the config for fluentbits for k8s and elasticsearch
    ```
    kubectl apply -f ./fluentbits/fluent-bit-configmap.yaml
    ```
1. Install the fluentbit daemonset
    ```
    kubectl apply -f ./fluentbits/fluent-bit-ds.yaml
    ```
1. Check to see that all pods are deployed and running
    ```
    kubectl get pods --namespace=logging
    ```
1. Look in elasticsearch for logs

####Next Steps
 Prometheus and Instrumentation
 
####Reference
the configs were based on 
`git@github.com:fluent/fluent-bit-kubernetes-logging.git` the only thing changed was the fluent-bit-ds.yaml
 file, specifically the image (since the default fluentbits image does not support arm) and the FLUENT_ELASTICSEARCH_HOST