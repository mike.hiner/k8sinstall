# Setup NFS shares that will be used by Prometheus and Grafana

####Synopsis
see title

####Result
A pair of NFS shares and the corresponding Persistent Volumes will be created
####Steps
1. Create the NFS directories for the shares.  **NOTE:** This makes the share globally readwrite.  this can probably be narrowed down but I have not researched it (I have not used NFS that much in the past)
    ```
    sudo mkdir /var/nfs
    sudo mkdir /var/nfs/vol1
    sudo mkdir /var/nfs/vol2
    chown -R og+rwx /var/nfs
    
    ```
    
1. install nfs server
    ```
    sudo apt-get install nfs-kernel-server
    ```
1. Create or update the /etc/exports file with the following lines:
    ```
    /var/nfs/vol1   192.168.0.0/24(rw,sync,no_subtree_check)
    /var/nfs/vol2   192.168.0.0/24(rw,sync,no_subtree_check)
    ```
1. Restart the nfs server
    ```
    sudo systemctl restart nfs-kernel-server
    ```    
1. Update the ip address for the volumes and add the nfs shares as persistent volumes:
    ```
    # UPDATE IP ADDRESS FIRST
    kubectl apply -f ./pv1.yml
    kubectl apply -f ./pv2.yml
    ```

# Install Prometheus and Grafana

####Synopsis
In this step we install Prometheus and Grafana for instrumentation
####Result
Prometheus and Grafana running on the k8s cluster

####Steps
1. Unzip the prometheus operator zip file.
1. on all nodes (master and worker) run the following commands (see readme in zip for explanation)
    ```
    # Enable cadvisor port
    sudo sed -e "/cadvisor-port=0/d" -i /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
     # Enable Webhook authorization
    sudo perl -pi -e "s/(?:--authentication-token-webhook=true )*--authorization-mode=Webhook/--authentication-token-webhook=true sudo systemctl daemon-reload
    sudo systemctl restart kubelet
    ```
1. on the master node run the following commands (see readme in zip for explanation)
    ```
    sudo sed -e "s/- --address=127.0.0.1/- --address=0.0.0.0/" -i /etc/kubernetes/manifests/kube-controller-manager.yaml
    sudo sed -e "s/- --address=127.0.0.1/- --address=0.0.0.0/" -i /etc/kubernetes/manifests/kube-scheduler.yaml
    ```
1. Update the configuration to suit.  Specifically URLs but also email.  For convenience I attached a diff from the stock deployment at prom.diff
1. Set entries in your /etc/hosts file to point to the URLs you specified
1. run ./deploy in the root dir of the zip
1. Give it a few minutes and see if the pods are all up
    ```
    kubectl get pods --namespace=monitoring
    ```
1. Go to the URL specified for Grafana and login.
1. Add more dashboards

####Next Steps
 Jaeger | Zipkin and distributed tracing
 
####Reference
Installing an nfs server on Ubuntu:
* https://linuxconfig.org/how-to-configure-a-nfs-file-server-on-ubuntu-18-04-bionic-beaver