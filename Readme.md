## Install material

This project is broken down by steps

1. Install OS
    1. copy baseline image
    1. Install Dependencies
    1. update machine-id
1. k8s install using kubeadm
1. Install flannel
1. Create first deployment
1. Install traefik daemonset
1. Install Fluentd/bits
    1. create a elasticsearch server
    1. set up elasticseach service and endpoint
    1. install fluentbits
1. Install prometheus operator